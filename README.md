# README #

This is test task for Strevus.

### What is this repository for? ###

* It is calculator tool.
* Works with expressions represented by strings.
* Operations: +, -, /, *, ^, sin, cos, tg, sqrt, log.
* You can type with spaces like "sin(3) * 2 + 1" or "sin(3)*2+1", it's doesn't matter.


* run "mvn tets" to run test
* run "mvn cobertura:cobertura" to run test coverage tool and create report