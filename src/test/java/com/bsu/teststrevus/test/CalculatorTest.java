package com.bsu.teststrevus.test;

import com.bsu.teststrevus.calculation.Calculator;
import junit.framework.TestCase;

/**
 * Created by matvei on 22/06/14.
 */
public class CalculatorTest extends TestCase {
    public void testCalculate() {
        Calculator calculator = new Calculator();
        String simpleExpression = "4 + 3";
        String complexExpression = "12 + 13 * (tg(1) - 22 * sqrt(20)) / 7 + 2 ^ 2";

        double result = calculator.calculate(simpleExpression);
        assertEquals(result, 7.0);

        result = calculator.calculate(complexExpression);
        double expected = 12 + 13 * (Math.tan(1) - 22 * Math.sqrt(20)) / 7 + Math.pow(2, 2);
        assertEquals(expected, result);
    }
}
