package com.bsu.teststrevus.test;
/**
 * Created by matvei on 20/06/14.
 */
import com.bsu.teststrevus.calculation.Operations;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;


public class OperationTest extends TestCase{
    double a = 3.5;
    double b = 5.21;

    public void testSimpleOperations(){
        double result = a + b;
        assertTrue(String.format("%f * %f should be %f\n", a, b, result), result == Operations.sum(a, b));

        result = a - b;
        assertTrue(String.format("%f * %f should be %f\n", a, b, result), result == Operations.diff(a, b));


        result = a / b;
        assertTrue(String.format("%f / %f should be %f\n", a, b, result), result == Operations.divide(a, b));

        result = a * b;
        assertTrue(String.format("%f * %f should be %f\n", a, b, result), result == Operations.product(a, b));


        result = Math.sin(a);
        assertTrue(String.format("sin(%f) should be %f\n", a, result), result == Operations.sin(a));


        result = Math.cos(b);
        assertTrue(String.format("cos(%f) should be %f\n", a, result), result == Operations.cos(b));
    }

}
