package com.bsu.teststrevus.test;

import com.bsu.teststrevus.calculation.Operations;
import com.bsu.teststrevus.parsing.Parser;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matvei on 21/06/14.
 */
public class ParserTest extends TestCase {
    public void testSimpleOperationsParsing(){
        Parser parser = new Parser();
        String expr = "3 + 4";
        ArrayList<String> result = new ArrayList<String>();
        result.add("3");
        result.add("4");
        result.add("+");

        List parsed = parser.parse(expr);

        assertTrue(String.format("size of res should be %d but it is %d\n", result.size(), parsed.size()), result.size() == parsed.size());

        for (int i = 0; i < result.size(); i++) {
            assertTrue(String.format("%s should be %s\n", result.get(i), parsed.get(i)), result.get(i).equals(parsed.get(i)));
        }

        String expr1 = "sin(3)";
        result.clear();
        result.add("3");
        result.add("sin");
        parsed.clear();
        parsed = parser.parse(expr1);

        assertTrue(String.format("size of res should be %d but it is %d\n", result.size(), parsed.size()), result.size() == parsed.size());

        for (int i = 0; i < result.size(); i++) {
            assertTrue(String.format("%s should be %s\n", parsed.get(i), result.get(i)), result.get(i).equals(parsed.get(i)));
        }
    }

    public void testComplexOperations(){
        Parser parser = new Parser();
        String expr = "3 + (sin(3) * cos(4) + 5 * 3) / 2 + (21 - 13) * 1";
        ArrayList<String> result = new ArrayList<String>();
        result.add("3");
        result.add("3");
        result.add("sin");
        result.add("4");
        result.add("cos");
        result.add("*");
        result.add("5");
        result.add("3");
        result.add("*");
        result.add("+");
        result.add("2");
        result.add("/");
        result.add("+");
        result.add("21");
        result.add("13");
        result.add("-");
        result.add("1");
        result.add("*");
        result.add("+");

        List<String> parsed = parser.parse(expr);

        assertTrue(String.format("size of res should be %d but it is %d\n", result.size(), parsed.size()), result.size() == parsed.size());

        for (int i = 0; i < result.size(); i++) {
            assertTrue(String.format("%s should be %s\n", result.get(i), parsed.get(i)), result.get(i).equals(parsed.get(i)));
        }
    }

}
