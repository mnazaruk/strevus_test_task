package com.bsu.teststrevus.test;

import com.bsu.teststrevus.parsing.FileReader;
import com.bsu.teststrevus.parsing.FileWriter;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by matvei on 22/06/14.
 */
public class ReaderWriterTest extends TestCase {
    public void testReadFile() {
        String filename = "src/input.txt";
        ArrayList<String> lines = new ArrayList<String>();
        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(filename));
            String line = reader.readLine();
            lines.add(line);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileReader freader = new FileReader(filename);

        for (String line : lines) {
            assertEquals(line, freader.nextExpression());
        }

        freader.close();
    }

    public void testWriteFile() {
        String filename = "src/output.txt";
        List<String> exps = Arrays.asList("1+1", "2+2");
        List<String> res = Arrays.asList("2", "4");

        FileWriter writer = new FileWriter(filename);

        for (int i = 0; i < exps.size(); i++) {
            writer.writeExpression(exps.get(i), res.get(i));
        }

        writer.close();

        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(filename));
            for (int i = 0; i < exps.size(); i++) {
                String line  = reader.readLine();
                assertNotNull(line);

                assertEquals(exps.get(i) + "=" + res.get(i), line);
            }
         } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
