package com.bsu.teststrevus.test;

import com.bsu.teststrevus.calculation.CalculationManager;
import com.bsu.teststrevus.calculation.Calculator;
import com.bsu.teststrevus.parsing.FileReader;
import junit.framework.TestCase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by matvei on 22/06/14.
 */
public class CalculationManagerTest extends TestCase {
    public void testManager() {
        Calculator calculator = new Calculator();
        String in = "src/input.txt";
        String out = "src/output.txt";

        ArrayList<String> eqs = new ArrayList<String>();
        ArrayList<String> expected = new ArrayList<String>();
        FileReader reader = new FileReader(in);
        String line = reader.nextExpression();
        while (line != null) {
            eqs.add(line);
            line = reader.nextExpression();
        }
        reader.close();

        for (String eq : eqs) {
            expected.add(eq + "=" + calculator.calculate(eq));
        }

        CalculationManager calculationManager = new CalculationManager(in, out);
        calculationManager.performCalculations();

        try {
            BufferedReader reader1 = new BufferedReader(new java.io.FileReader(out));
            for (int i = 0; i < expected.size(); i++) {
                String resLine  = reader1.readLine();
                assertNotNull(resLine);

                assertEquals(expected.get(i), resLine);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
