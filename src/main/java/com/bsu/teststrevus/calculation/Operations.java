package com.bsu.teststrevus.calculation;

import sun.nio.cs.ext.MacThai;

/**
 * Created by matvei on 20/06/14.
 */
public class Operations {
    public static double product(double a, double b) {
        return a * b;
    }

    public static double sum(double a, double b) {
        return a + b;
    }

    public static double diff(double a, double b) {
        return a - b;
    }

    public static double divide(double a, double b) {
        return a / b;
    }

    public static double sin(double a) {
        return Math.sin(a);
    }

    public static double cos(double a) {
        return Math.cos(a);
    }

    public static double tg(double a) {return Math.tan(a); }

    public static double pow(double a, double b) { return Math.pow(a, b); }

    public static double sqrt(double a) { return Math.sqrt(a); }

    public static double log(double a) { return Math.log(a); }
}
