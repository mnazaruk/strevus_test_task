package com.bsu.teststrevus.calculation;

import com.bsu.teststrevus.parsing.Parser;

import java.util.List;
import java.util.Stack;

/**
 * Created by matvei on 22/06/14.
 */
public class Calculator {
    Parser parser = new Parser();
    Stack<Double> numbersStack = new Stack<Double>();


    public double calculate(String expression){
        List<String> rpnExpression = parser.parse(expression);

        for (String element : rpnExpression) {
//            for (Double el : numbersStack) {
//                System.out.print(el + " : ");
//            }
//            System.out.println();
//            System.out.println(element);
            try {
                if (isNumber(element)) {
                    numbersStack.push(Double.parseDouble(element));
                } else {
                    numbersStack.push(performOperation(element));
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }

        return numbersStack.pop();
    }

    private double performOperation(String operation){
        Double result = 0.0;
        if (operation.equals("+")) {

            Double b = numbersStack.pop();
            Double a = numbersStack.pop();
            result = Operations.sum(a, b);

        } else if (operation.equals("-")) {

            Double b = numbersStack.pop();
            Double a = numbersStack.pop();
            result = Operations.diff(a, b);

        } else if (operation.equals("*")) {

            Double b = numbersStack.pop();
            Double a = numbersStack.pop();
            result = Operations.product(a, b);

        } else if (operation.equals("/")) {

            Double b = numbersStack.pop();
            Double a = numbersStack.pop();
            result = Operations.divide(a, b);

        } else if (operation.equals("sin")) {
            Double a = numbersStack.pop();
            result = Operations.sin(a);

        } else if (operation.equals("cos")) {

            Double a = numbersStack.pop();
            result = Operations.cos(a);

        } else if (operation.equals("tg")) {

            Double a = numbersStack.pop();
            result = Operations.tg(a);

        } else if (operation.equals("^")) {

            Double b = numbersStack.pop();
            Double a = numbersStack.pop();
            result = Operations.pow(a, b);

        } else if (operation.equals("sqrt")) {

            Double a = numbersStack.pop();
            result = Operations.sqrt(a);

        }else if (operation.equals("log")) {

            Double a = numbersStack.pop();
            result = Operations.log(a);

        }
        return result;
    }

    private boolean isNumber(String element) {
        for (char c : element.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }
}
