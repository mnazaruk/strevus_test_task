package com.bsu.teststrevus.calculation;

import com.bsu.teststrevus.parsing.FileReader;
import com.bsu.teststrevus.parsing.FileWriter;

import java.util.ArrayList;

/**
 * Created by matvei on 22/06/14.
 */
public class CalculationManager {
    Calculator calculator = new Calculator();
    String inputFilename;
    String outputFilename;
    ArrayList<String> equations = new ArrayList<String>();
    ArrayList<String> results = new ArrayList<String>();

    public CalculationManager(String inputFilename, String outputFilename) {
        this.inputFilename = inputFilename;
        this.outputFilename = outputFilename;
    }

    public void performCalculations() {
        load();

        for (String equation : equations) {
            results.add("" + calculator.calculate(equation));
        }
        save();

        equations.clear();
        results.clear();
    }

    private void load() {
        FileReader reader = new FileReader(inputFilename);
        String expression = reader.nextExpression();

        while (expression != null) {
            equations.add(expression);
            expression = reader.nextExpression();
        }

        reader.close();
    }

    private void save() {
        FileWriter writer = new FileWriter(outputFilename);

        for (int i = 0; i < equations.size(); i++) {
            writer.writeExpression(equations.get(i), results.get(i));
        }

        writer.close();
    }
}
