package com.bsu.teststrevus.parsing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by matvei on 21/06/14.
 */
public class Parser {


    private List<String> single_operand_operators = Arrays.asList("sin", "cos");
    private List<String> standart_operators = Arrays.asList("(", ")", "+", "-", "*", "/", "^", "sin", "cos", "tg", "log", "sqrt");

    private String processNumber(String expression, int i) {
        StringBuilder number = new StringBuilder("");
        while (i < expression.length() && Character.isDigit(expression.charAt(i))) {
            number.append(expression.charAt(i++));
        }
        return number.toString();
    }

    private String processLetter(String expression, int i) {
        StringBuilder operator = new StringBuilder("");
        while (i < expression.length() && Character.isLetter(expression.charAt(i))) {
            operator.append(expression.charAt(i++));
        }
        return operator.toString();
    }

    private List<String> separate (String expression){
        ArrayList<String> separated = new ArrayList<String>();

        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if (c == ' ') {
                continue;
            }
            if (Character.isDigit(c)) {
                String number = processNumber(expression, i);
                separated.add(number.toString());
                i += number.length() - 1;
            } else if (Character.isLetter(c)) {
                String operator = processLetter(expression, i);
                separated.add(operator);
                i += operator.length() - 1;
            } else {
                separated.add(String.format("%c", c));
            }
        }
        return separated;
    }

    private boolean isOperation(String operation){
        for (String op : standart_operators) {
            if (op.equalsIgnoreCase(operation)) {
                return true;
            }
        }
        return false;
    }

    private int priority(String s) {
        if (!isOperation(s)) {
            return -1;
        }
        if (s.equals("(")) {
            return 0;
        } else if (s.equals(")")) {
            return 1;
        } else if (s.equals("+")) {
            return 2;
        } else if (s.equals("-")) {
            return 3;
        } else if (s.equals("*") || s.equals("/")) {
            return 4;
        } else if (s.equals("^")) {
            return 5;
        } else {
            return 6;
        }
    }



    public List<String> parse(String expression) {
        ArrayList<String> result = new ArrayList<String>();
        Stack<String> operations = new Stack<String>();

        for (String s : separate(expression)) {
            if (isOperation(s)) {
                if (operations.size() > 0 && !s.equals("(")) {
                    if (s.equals(")")) {
                        while (!operations.peek().equals("(")) {
                            result.add(operations.pop());
                        }
                        operations.pop();
                    } else if (priority(s) > priority(operations.peek())){
                        operations.push(s);
                    } else {
                        while (operations.size() > 0 && priority(s) <= priority(operations.peek())) {
                            result.add(operations.pop());
                        }
                        operations.push(s);
                    }
                } else {
                    operations.push(s);
                }
            } else {
                result.add(s);
                if (operations.size() > 0 && isSingleOpearandOperation(operations.peek())) {
                    result.add(operations.pop());
                }
            }
        }

        if (operations.size() > 0) {
            while (operations.size() > 0) {
                result.add(operations.pop());
            }
        }
        return result;
   }

    private boolean isSingleOpearandOperation(String s) {
        for (String op : single_operand_operators) {
            if (op.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }
}
