package com.bsu.teststrevus.parsing;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Created by matvei on 22/06/14.
 */
public class FileWriter {
    BufferedWriter writer;

    public FileWriter (String filename) {
        try {
            writer = new BufferedWriter(new java.io.FileWriter(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeExpression(String expression, String result){
        String resLine = expression + "=" + result + "\n";
        try {
            writer.write(resLine);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
