package com.bsu.teststrevus.parsing;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

/**
 * Created by matvei on 22/06/14.
 */
public class FileReader {
    BufferedReader reader;

    public FileReader(String filename) {
        try {
            reader = new BufferedReader(new java.io.FileReader(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String nextExpression () {
        String expression = null;
        try {
            expression = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return expression;
    }

    public void close(){
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
